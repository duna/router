<?php

namespace Duna\Router\Bridges\Tracy;
use Nette\Application\IRouter;
use Nette\Http\IRequest;
use Tracy\Dumper;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 * @package Duna\Router\Bridges\Tracy
 */
class TracyPanel implements \Tracy\IBarPanel
{

    private $appRequest;
    private $httRequest;
    private $router;

    public function __construct(IRequest $httpRequest, IRouter $router)
    {
        $this->httRequest = $httpRequest;
        $this->router = $router;
        $this->appRequest = $router->match($this->httRequest);
    }

    public function getPanel()
    {
        ob_start();
        $url = $this->httRequest->getUrl();
        $request = Dumper::toHtml($this->appRequest);
        require __DIR__ . '/templates/panel.phtml';
        return ob_get_clean();
    }

    public function getTab()
    {
        ob_start();
        $request = $this->appRequest;
        require __DIR__ . '/templates/tab.phtml';
        return ob_get_clean();
    }

}
