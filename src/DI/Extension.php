<?php

namespace Duna\Router\DI;

use App;
use Duna\Console\IEntityConsoleProvider;
use Duna\DI\Extensions\CompilerExtension;
use Duna\Plugin\Manager\Entity\Plugin;
use Duna\Plugin\Manager\IPlugin;
use Duna\Router\Cli\InstallCommand;
use Kdyby\Doctrine\DI\IEntityProvider;
use Nette\PhpGenerator\ClassType;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 * @package Duna\Router\DI
 */
class Extension extends CompilerExtension implements IPlugin, IEntityProvider, IEntityConsoleProvider
{


    // TODO Use this namespace in other places
    const CACHE_NAMESPACE = "duna.router";

    private $commands = [
        InstallCommand::class,
    ];

    public function afterCompile(ClassType $class)
    {
        $builder = $this->getContainerBuilder();
        $initialize = $class->getMethod('initialize');

        $initialize->addBody($builder->formatPhp('?;', [
            new \Nette\DI\Statement('@Tracy\Bar::addPanel', [new \Nette\DI\Statement('Duna\Router\Bridges\Tracy\TracyPanel')]),
        ]));
    }

    function getEntityMappings()
    {
        return self::entityMappings();
    }

    public function loadConfiguration()
    {
        $builder = $this->getContainerBuilder();
        $this->parseConfig(__DIR__ . '/config.neon');
        // Remove default Nette router
        $builder->removeDefinition('routing.router');

        $this->addCommands($this->commands);
    }

    /**
     * {@inheritDoc}
     */
    public static function getPluginInfo()
    {
        $entity = new Plugin();
        $entity->name = 'Router';
        $entity->description = 'Plugin router';

        return [
            'plugin' => $entity,
        ];
    }

    public static function entityMappings()
    {
        return ["Duna\\Router\\Entity" => __DIR__ . '/../Entity'];
    }

}
