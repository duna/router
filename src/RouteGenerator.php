<?php

namespace Duna\Router;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 * @package Duna\Router
 */
class RouteGenerator
{

	public static function generate($link, $destination)
	{
		$url = new Entity\Url;

		$url->setLink($link);
		$url->setDestination($destination);

		return $url;
	}

}
