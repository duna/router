<?php

namespace Duna\Router\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="plugin_url", uniqueConstraints={@ORM\UniqueConstraint(name="link", columns={"link", "localization_id"}), @ORM\UniqueConstraint(name="link", columns={"link", "localization_id"})})
 * @ORM\Entity
 */
class Url
{

    use \Kdyby\Doctrine\Entities\MagicAccessors;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=64, nullable=false)
     */
    protected $link;
    /**
     * @var string
     *
     * @ORM\Column(name="presenter", type="string", length=64, nullable=false)
     */
    protected $presenter = null;
    /**
     * @var string
     *
     * @ORM\Column(name="action", type="string", length=16, nullable=false)
     */
    protected $action = null;
    /**
     * @var integer
     *
     * @ORM\Column(name="internal_id", type="integer", nullable=true)
     */
    protected $internalId = null;
    /**
     * @var \Duna\Plugin\Localization\Entity\Localization
     *
     * @ORM\ManyToOne(targetEntity="\Duna\Plugin\Localization\Entity\Localization")
     * @ORM\JoinColumn(name="localization_id", referencedColumnName="localization_id", onDelete="cascade")
     */
    protected $localization;
    /**
     * @ORM\ManyToOne(targetEntity="Url")
     * @ORM\JoinColumn(name="redirect_to", referencedColumnName="url_id", onDelete="SET NULL")
     */
    protected $redirectTo = null;
    /**
     * @var integer
     *
     * @ORM\Column(name="url_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    public function getAbsoluteDestination($withAsterisk = false)
    {
        if (!isset($this->presenter) || !isset($this->action))
            return null;
        return ':' . $this->getPresenter() . ':' . ($withAsterisk ? '*' : $this->getAction());
    }

    public function getDestination()
    {
        return $this->getPresenter() . ':' . $this->getAction();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setDestination($presenter, $action = null)
    {
        $parsed = self::parseDestination($presenter, $action);
        $this->setPresenter($parsed[0])
            ->setAction($parsed[1]);
        return $this;
    }

    public function setLink($link)
    {
        $this->link = trim($link, " \t\n\r\0\x0B/");
        return $this;
    }

    public static function parseDestination($presenter, $action = null)
    {
        $destination = $presenter;
        if ($action !== null) {
            $destination .= ':' . $action;
        }
        $pos = strrpos($destination, ':');
        return [substr($destination, 0, $pos), substr($destination, $pos + 1)];
    }

}
