<?php

namespace Duna\Router;

use Duna\Plugin\Localization\Entity\Localization;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\IRouter;
use Nette\Application\Request;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\Http\IRequest;
use Nette\Http\Url;
use Nette\InvalidStateException;
use Nette\Utils\AssertionException;
use Nette\Utils\Strings;
use Tracy\Debugger;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 * @package Duna\Router
 */
class Router extends RouteList
{

    const CACHE_NAMESPACE = 'Duna.Router';

    /** @var EntityManager */
    private $em;

    /** @var \Nette\Caching\Cache */
    private $cache;
    private $flags;
    private $extension = '';
    private $allowedLanguages = [];
    private $paramsTanslateTable = [];

    /** @var \Nette\Http\Url|null */
    private $lastRefUrl;

    /** @var \Nette\Http\Url|null */
    private $lastBaseUrl;

    public function __construct(EntityManager $em, IStorage $cacheStorage)
    {
        $this->em = $em;
        $this->cache = new Cache($cacheStorage, self::CACHE_NAMESPACE);
        $this->flags = Route::$defaultFlags;

        if (PHP_SAPI === 'cli')
            return;

        $this->allowedLanguages = $this->cache->load('allowedLanguages', function () {
            return $this->em->getRepository(Localization::class)->findPairs('default', 'code');
        });
    }

    public function constructUrl(Request $appRequest, Url $refUrl)
    {
        if ($this->flags & self::ONE_WAY)
            return null;

        $cacheResult = $this->cache->load($appRequest, function (&$dep) use ($appRequest) {
            $path = null;
            $fallBack = false;
            $param = $appRequest->getParameters();
            $presenter = $appRequest->getPresenterName();
            $action = $param['action'];
            $internalId = array_key_exists('id', $param) ? $param['id'] : null;

            if (array_key_exists('id', $param)) {
                $path = $this->em->getRepository(\Duna\Router\Entity\Url::class)->findOneBy([
                    'presenter'  => $presenter,
                    'action'     => $action,
                    'internalId' => $internalId,
                ]);
                if ($path === null) {
                    $fallBack = true;
                    Debugger::log(new InvalidStateException(sprintf('Cannot find cool rout for destination %s. Fallback will be used. Internal ID is %s.', $presenter . ':' . $action, $internalId)));
                }
            }
            if (!array_key_exists('id', $param) || $fallBack) {
                $path = $this->em->getRepository(\Duna\Router\Entity\Url::class)->findOneBy([
                    'presenter' => $presenter,
                    'action'    => $action,
                ]);
            }
            if ($path === null) {
                Debugger::log(new InvalidStateException('Route for ' . $presenter . ':' . $action . ' not founded'), Debugger::CRITICAL);
                return null;
            }
            $dep = [Cache::TAGS => [self::CACHE_NAMESPACE . '/route/' . $path->getId()]];
            return [$path, $fallBack];
        });
        $path = $cacheResult[0];
        if ($path === null)
            return null;

        $params = $appRequest->getParameters();
        if ($this->lastRefUrl !== $refUrl) {
            $scheme = ($this->flags & self::SECURED ? 'https://' : 'http://');
            $this->lastBaseUrl = $scheme . $refUrl->getAuthority() . $refUrl->getBasePath();
            $this->lastRefUrl = $refUrl;
        }

        if ($path->getRedirectTo() === null) {
            $link = $path->getLink();
        } else {
            $link = $path->getRedirectTo()->getLink();
        }

        $locale = array_key_exists('locale', $params) && $params['locale'] && !$this->allowedLanguages[$params['locale']] ? $params['locale'] . '/' : null;
        $url = $this->lastBaseUrl . $locale . Strings::webalize($link, '/');
        unset($params['locale']);
        if (substr($url, -1) !== '/')
            $url .= $this->extension;

        unset($params['action']);
        if (!$cacheResult[1])
            unset($params['id']);

        foreach ($params as $key => $_) {
            if (array_key_exists($key, $this->paramsTanslateTable)) {
                $params[$this->paramsTanslateTable[$key]] = $params[$key];
                unset($params[$key]);
            }
        }

        $sep = ini_get('arg_separator.input');
        $query = http_build_query($params, '', $sep ? $sep[0] : '&');
        if ($query != '')
            $url .= '?' . $query;
        return $url;
    }

    public function match(IRequest $httpRequest)
    {
        foreach ($this as $route) {
            $appRequest = $route->match($httpRequest);
            if ($appRequest !== null)
                return $appRequest;
        }

        $url = $httpRequest->getUrl();
        $host = $url->getHost();
        $basepath = $url->getBasePath();
        if (strncmp($url->getPath(), $basepath, strlen($basepath)) !== 0)
            return null;

        $path = (string) substr($url->getPath(), strlen($basepath));
        if ($path !== '')
            $path = rtrim(rawurldecode($path), '/');

        $path = preg_replace('/(' . preg_quote('.html', '/') . '|' . preg_quote('.htm', '/') . ')$/', '', $path);

        $locale = null;
        $re = '/^(' . implode('|', array_keys($this->allowedLanguages)) . ')/';
        if (preg_match('/^(' . implode('|', array_keys($this->allowedLanguages)) . ')/', $path, $matches))
            $locale = $matches[1];
        else
            $locale = array_search(true, $this->allowedLanguages);
        $path = preg_replace($re, '', $path);

        //$path = (string) preg_replace('~' . preg_quote($this->, $delimiter), $host, $url);

        $destination = $this->cache->load($path, function (&$dep) use ($path) {
            /** @var \Duna\Router\Entity\Url */
            $destination = $this->em->getRepository(\Duna\Router\Entity\Url::class)->findOneBy(['link' => $path]);
            if ($destination === null) {
                Debugger::log(new InvalidStateException(sprintf('Cannot find route for path %s', $path)));
                return null;
            }
            $dep = [Cache::TAGS => [self::CACHE_NAMESPACE . '/router/' . $destination->getId()]];
            return $destination;
        });
        if ($destination === null)
            return null;

        if ($destination->getRedirectTo() === null) {
            $internalDestination = $destination->getDestination();
            $internalId = $destination->getInternalId();
        } else {
            $internalDestination = $destination->getRedirectTo()->getDestination();
            $internalId = $destination->getRedirectTo()->getInternalId();
        }
        $pos = strrpos($internalDestination, ':');
        $presenter = substr($internalDestination, 0, $pos);
        $action = substr($internalDestination, $pos + 1);

        $params = $httpRequest->getQuery();
        foreach ($params as $key => $_) {
            $translateTable = array_flip($this->paramsTanslateTable);
            if (array_key_exists($key, $translateTable)) {
                $params[$translateTable[$key]] = $params[$key];
                unset($params[$key]);
            }
        }

        $params['action'] = $action;
        $params['locale'] = $locale;
        if ($internalId)
            $params['id'] = $internalId;
        return new Request($presenter, $httpRequest->getMethod(), $params, $httpRequest->getPost(), $httpRequest->getFiles(), [Request::SECURED => $httpRequest->isSecured()]
        );
    }

    /**
     * @param string $extension
     */
    public function setExtension($extension)
    {
        //TODO dodělat $this->extension = $this->facade->getOption('page_url_end');
        $this->extension = $extension;
    }

    public static function prependTo(IRouter &$router, IRouter $newRouter)
    {
        if (!$router instanceof RouteList)
            throw new AssertionException('Router must be an instance of Nette\Application\Routers\RouteList');

        $router[] = $newRouter;
        $last = count($router) - 1;
        foreach ($router as $i => $route) {
            if ($i === $last)
                break;
            $router[$i + 1] = $route;
        }
        $router[0] = $newRouter;
    }

}
