<?php

namespace Duna\Router\Facade;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Duna\Plugin\Localization\Entity\Localization;
use Duna\Plugin\Localization\Facade;
use Duna\Router\Entity\Url;
use Nette\InvalidStateException;

class UrlFacade
{

    /** @var \Doctrine\ORM\EntityManagerInterface */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getById($id, $throwException = false)
    {
        $entity = $this->em->getRepository(Url::class)->find($id);

        if ($entity === null && $throwException)
            throw new InvalidStateException();

        return $entity;
    }

    /**
     * @param string $link
     * @param string $presenter
     * @param mixed  $localization
     * @param string $action
     * @param bool   $throwException
     * @return \Duna\Router\Entity\Url
     */
    public function insert($link, $presenter, $localization, $action = null, $throwException = false)
    {
        $entity = new Url();
        $entity->setLink($link);
        $entity->setDestination($presenter, $action);
        if ($localization === null) {
        } elseif ($localization instanceof Localization) {
            $entity->localization = $localization;
        } elseif (is_numeric($localization)) {
            $facade = new Facade($this->em);
            $entity->localization = $facade->getById((int) $localization, true);
        } else {
            $facade = new Facade($this->em);
            $entity->localization = $facade->getOneByCode($localization, true);
        }

        $this->em->persist($entity);
        try {
            $this->em->flush($entity);
            return $entity;
        } catch (UniqueConstraintViolationException $e) {
            if ($throwException)
                throw  new InvalidStateException();
        }
    }

    public function deleteByLink($link, $localization)
    {
        try {
            $entity = $this->getBy($link, $localization, true);
            $this->em->remove($entity);
            $this->em->flush($entity);
        } catch (InvalidStateException $e) {
        }
    }

    public function getBy($link, $localization, $throwException = false)
    {
        $localizationEntity = null;
        try {
            if ($localization === null) {
                $localizationEntity = null;
            } elseif ($localization instanceof Localization) {
                $localizationEntity = $localization;
            } elseif (is_numeric($localization)) {
                $facade = new Facade($this->em);
                $localizationEntity = $facade->getById((int) $localization, true);
            } else {
                $facade = new Facade($this->em);
                $localizationEntity = $facade->getOneByCode($localization, true);
            }
            $entity = $this->em->getRepository(Url::class)->findOneBy([
                'link'         => $link,
                'localization' => $localizationEntity,
            ]);

            if ($entity === null && $throwException)
                throw new InvalidStateException();

            return $entity;
        } catch (InvalidStateException $e) {
            if ($throwException)
                throw  new InvalidStateException();
        }
        return null;
    }
}
